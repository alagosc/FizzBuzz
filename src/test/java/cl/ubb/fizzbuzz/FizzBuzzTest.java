package cl.ubb.fizzbuzz;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FizzBuzzTest {
	
	private FizzBuzz fb;
	private String resultado;
	
	@Before
	public void SetUp(){
		fb = new FizzBuzz();
				
	}
	
	@Test
	public void IngresarNumeroUnoRetornarNumeroUno() {
		/*arrange*/
	//	FizzBuzz fb = new FizzBuzz();
	//	String resultado; 
		
		/*act*/
		resultado = fb.juego(1);
		
		/*assert*/
		assertEquals(resultado, "1");
	}


	@Test
	public void IngresarNumeroDosRetornarNumeroDos() {
		/*arrange*/
	//	FizzBuzz fb = new FizzBuzz();
	//	String resultado; 
		
		/*act*/
		resultado = fb.juego(2);
		
		/*assert*/
		assertEquals(resultado, "2");
	}

	@Test
	public void IngresarNumeroTresRetornarFizz() {
		/*arrange*/
	//	FizzBuzz fb = new FizzBuzz();
	//	String resultado; 
		
		/*act*/
		resultado = fb.juego(3);
		
		/*assert*/
		assertEquals(resultado, "Fizz");
		
	}
	@Test
	public void IngresarNumeroCincoRetornarBuzz() {
		/*arrange*/
		//FizzBuzz fb = new FizzBuzz();
		//String resultado; 
		
		/*act*/
		resultado = fb.juego(5);
		
		/*assert*/
		assertEquals(resultado, "Buzz");
		
	}
	
}
